# -*- coding: utf-8 -*-

import routing
import logging
import os
import xbmcaddon
import xbmcgui
import xbmcplugin
import urllib

from resources.lib import kodiutils
from resources.lib import kodilogging

from xbmcgui import ListItem
from xbmcplugin import addDirectoryItem, endOfDirectory

import xml.etree.ElementTree as ET

ADDON = xbmcaddon.Addon()
logger = logging.getLogger(ADDON.getAddonInfo('id'))
kodilogging.config()
plugin = routing.Plugin()


playlist_filename = "radioitaliastreaming.xspf"
playlist_url = "http://www.radioitaliastreaming.it/" + playlist_filename
ns = {"xspf": "http://xspf.org/ns/0/"}
userdata = os.path.join(os.path.expanduser("~"),
                        ".kodi/userdata/addon_data/",
                        ADDON.getAddonInfo('id'))
playlist_file = os.path.join(userdata, playlist_filename)

# Container for all radios
radio = {}


@plugin.route('/')
def index():
    """It shows the radio's groups. Radios are grouped by creator"""
    for catid, catname in enumerate(radio):
        addDirectoryItem(plugin.handle, plugin.url_for(
            show_category, catid), ListItem(catname), True)
    endOfDirectory(plugin.handle)


@plugin.route('/category/<category_id>')
def show_category(category_id):
    """It shows the radio within a group"""
    radio_group = []
    for catid, catname in enumerate(radio):
        if category_id == str(catid):
            radio_group = radio[catname]

    for r in radio_group:
        addDirectoryItem(plugin.handle,
                         r["url"],
                         ListItem("{}".format(r["title"])))
    endOfDirectory(plugin.handle)


def get_page():
    """It downloads the XSPF playlist file containing all the radio sources"""
    try:
        os.makedirs(userdata)
    except OSError:
        if not os.path.isdir(userdata):
            raise
    try:
        urllib.urlretrieve(playlist_url, playlist_file)
    except Exception as e:
        kodiutils.notification("Radio Italia Streaming",
                               "Impossibile scaricare la lista aggiornata delle radio")
        logger.warning("Impossible to download: {}".format(playlist_url))

    if os.path.exists(playlist_file):
        return playlist_file
    else:
        logger.error("Cannot find XSPF playlist {}".format(playlist_file))
        raise Exception("Impossibile trovare la lista di radio")


def get_radio_list():
    """It converts an XSPF playlist to a Python list/dictionary"""
    tree = ET.parse(get_page())
    playlist = tree.getroot()

    for track in playlist.findall('xspf:trackList/xspf:track', ns):
        title_el = track.find("xspf:title", ns)
        location_el = track.find("xspf:location", ns)
        image_el = track.find("xspf:image", ns)
        creator_el = track.find("xspf:creator", ns)

        if title_el is None or location_el is None:
            continue

        title = title_el.text.encode('utf8')
        location = location_el.text.encode('utf8')

        image = 'DefaultAudio.png'
        if image_el is not None and image_el.text is not None:
            image = image_el.text

        creator = "altre"
        if creator_el is not None and creator_el.text is not None:
            creator = creator_el.text

        if creator not in radio:
            radio[creator] = []
        radio[creator].append({'title': title,
                               'url': location,
                               'image': image})
    return radio


def run():
    radio = get_radio_list()
    logger.error(str(radio))
    plugin.run()
