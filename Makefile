NAME := plugin.audio.radioitaliastreaming
DEST_DIR = $(HOME)/.kodi/addons/$(NAME)
FILES := CHANGELOG LICENSE README.rst
FILES += addon.xml main.py
DIRS := resources
PNG := radioitaliastreaming.png

package:
	@rm -f $(NAME).zip
	@mkdir -p $(NAME)
	@cp $(FILES) $(NAME)
	@cp -a $(DIRS) $(NAME)
	@zip -r $(NAME).zip $(NAME)
	@rm -rf $(NAME)

install:
	@mkdir -p $(DEST_DIR)
	@cp -a $(DIRS) $(DEST_DIR)
	@cp $(FILES) $(DEST_DIR)

uninstall:
	@rm -rf $(DEST_DIR)/$(DIR)


.PHONY: install uninstall
