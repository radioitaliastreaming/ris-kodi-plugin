Kodi Addon Radio Italia Streaming
=================================

Attraverso questa estensione per `Kodi`_ è possibile accedere alle
radio di `Radio Italia Streaming`_.

Installazione
-------------

Utilizzando i sorgenti
~~~~~~~~~~~~~~~~~~~~~~
Per installare l'estensione dai sorgenti clonate il repositorio git dalla
`pagina del progetto`_ ed utilizzare *make*. Di seguito un esempio.::

    git clone https://gitlab.com/radioitaliastreaming/ris-kodi-plugin.git
    cd ris-kodi-plugin
    make install

.. _`Kodi`: https://kodi.tv/
.. _`Radio Italia Streaming`: http://www.radioitaliastreaming.it/
.. _`pagina del progetto`: https://gitlab.com/radioitaliastreaming/ris-kodi-plugin
